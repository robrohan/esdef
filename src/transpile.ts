
export type vec3 = [number?, number?, number?];
export type vec4 = [number?, number?, number?, number?];
export type color = vec4;
type reg = string;
type drawFn = (register: reg) => void;

export function scene(st: (() => string)[]) {
  return function () {
    let buffer = '';
    for (let x = 0; x < st.length; x++) {
      buffer += st[x]();
    }
    return buffer;
  };
}

export function sphere(pos: vec3, radius: vec4): drawFn {
  return function (register: reg) {
    return `${register} = pCallback(4, p, vec3(${pos[0]}, ${pos[1]}, ${pos[2]}), vec4(0., 0., 0., 1.), vec4(${radius[0]}, 0., 0., 0.));`;
  };
};

export function cube(pos: vec3, bounding: vec4): drawFn {
  return function (register: reg) {
    return `${register} = pCallback(5, p, vec3(${pos[0]}, ${pos[1]}, ${pos[2]}), vec4(0., 0., 0., 1.), vec4(${bounding[0]}, ${bounding[1]}, ${bounding[2]}, 0));`;
  };
};

/**
 * Draw a flat ground plane. Only Y matters here - the height
 */
export function ground(pos: vec3): drawFn {
  return function (register: reg) {
    return `${register} = pCallback(6, p, vec3(${pos[0]}, ${pos[1]}, ${pos[2]}), vec4(0., 0., 0., 1.), vec4(0.));`;
  };
};

export function capsule(pos1: vec3, pos2: vec4): drawFn {
  return function (register: reg) {
    return `${register} = pCallback(1, p, vec3(${pos1[0]}, ${pos1[1]}, ${pos1[2]}), vec4(0., 0., 0., 1.), vec4(${pos2[0]}, ${pos2[1]}, ${pos2[2]}, ${pos2[3]}));`;
  };
};

export function cylinder(pos1: vec3, pos2: vec4): drawFn {
  return function (register: reg) {
    return `${register} = pCallback(2, p, vec3(${pos1[0]}, ${pos1[1]}, ${pos1[2]}), vec4(0., 0., 0., 1.), vec4(${pos2[0]}, ${pos2[1]}, ${pos2[2]}, ${pos2[3]}));`;
  };
};

export function torus(pos: vec3, radius: vec4): drawFn {
  return function (register: reg) {
    return `${register} = pCallback(3, p, vec3(${pos[0]}, ${pos[1]}, ${pos[2]}), vec4(0., 0., 0., 1.), vec4(${radius[0]}, ${radius[1]}, 0., 0.));`;
  };
};

/** obj, draw "dynamic object" */
export function obj(idx: number): drawFn {
  return function (register: reg) {
    return `${register} = pCallback(objects[${idx}].type, p, objects[${idx}].pos,  objects[${idx}].quat, objects[${idx}].params);`
  }
}

// ////////////////////////////////////////////////

export function single() {
  return function () {
    return `p = ctx;`;
  };
}

export function repeat(blocks = [1, 1, 1]) {
  return function () {
    return `p = opRep(ctx, vec3(${blocks[0]}, ${blocks[1]}, ${blocks[2]}));`;
  };
}

export function draw(func: drawFn, color: vec4 = [.5, .5, .5, 1], register: reg = 'tmp0') {
  return function () {
    let b = '';
    const obj = func(register);

    b += obj;
    b += flush(color, register);
    return b;
  };
};

export function join(funcs: drawFn[]) {
  return function (r: reg) {
    let b = '';
    const fLen = funcs.length;
    if (fLen % 2 === 0) {
      throw new Error('Join can not have an even number of statements');
    }

    b += funcs[0](r);
    for (let x = 1; x < fLen; x++) {
      b += funcs[x](x % 2 == 0 ? r : 'tmp1');
    }

    return b;
  };
}

// ////////////////////////////////////////////////

export function combine(r1 = 'tmp0', r2 = 'tmp1') {
  return function () {
    return `${r1} = opCombine(${r1}, ${r2});`;
  };
}

export function union(r1 = 'tmp0', r2 = 'tmp1') {
  return function () {
    return `${r1} = opUnion(${r1}, ${r2});`;
  };
}

export function subtract(r1 = 'tmp0', r2 = 'tmp1') {
  return function () {
    return `${r1} = opSubtraction(${r1}, ${r2});`;
  };
}

export function intersect(r1 = 'tmp0', r2 = 'tmp1') {
  return function () {
    return `${r1} = opIntersection(${r1}, ${r2});`;
  };
}

// ////////////////////////////////////////////////

export function smoothUnion(param: vec4, r1 = 'tmp0', r2 = 'tmp1') {
  return function () {
    return `${r1} = opSmoothUnion(${r1}, ${r2}, ${param[0]});`;
  };
}

export function smoothSubtract(param: vec4, r1 = 'tmp0', r2 = 'tmp1') {
  return function () {
    return `${r1} = opSmoothSubtraction(${r1}, ${r2}, ${param[0]});`;
  };
}

export function smoothIntersect(param: vec4, r1 = 'tmp0', r2 = 'tmp1') {
  return function () {
    return `${r1} = opSmoothIntersection(${r1}, ${r2}, ${param[0]});`;
  };
}

// ////////////////////////////////////////////////

export function flush(color: color, register = 'tmp0') {
  return `
d = min(d, ${register});
if (d >= ${register}) col = vec4(${color[0]}, ${color[1]}, ${color[2]}, ${color[3]});
`;
}

// ////////////////////////////////////////////////

export function lights(lights: drawFn[]) {
  return function () {
    let lightBuffer = '';
    if (lights.length) {
      for (let x = 0; x < lights.length; x++) {
        lightBuffer += lights[x]('l' + x);
        lightBuffer += `float dif${x} = GetLight(p, n, l${x});`;
      }
      lightBuffer = flushLights(lights.length, lightBuffer);
    }
    return lightBuffer;
  };
}

export function light(pos: vec3, color: color) {
  return function (vari: reg) {
    return `Light ${vari} = Light(vec3(${pos[0]}, ${pos[1]}, ${pos[2]}), vec4(${color[0]}, ${color[1]}, ${color[2]}, ${color[3]}));`;
  };
}

export function flushLights(lightLen: number, buffer: string) {
  buffer += `col = col * (`;
  for (let x = 0; x < lightLen; x++) {
    buffer += `+ (l${x}.c * dif${x})`;
  }
  buffer += ` );`;

  return buffer;
}

// //////////////////////////////////

export function run(pg: (() => () => string)[]): [() => string, () => string] {
  const scene = pg[0]();
  const lights = pg[1]();
  return [scene, lights];
};
