
clean:
	rm -rf dist

watch:
	mkdir -p dist
	cp src/index.html dist/index.html
	npm run watch

build:
	mkdir -p dist
	cp src/index.html dist/index.html
	npm run build
