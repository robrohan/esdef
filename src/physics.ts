import { World, Body, Vec3, Plane, Sphere, Box, ContactMaterial, BodyOptions, Quaternion, Cylinder } from 'dapao';
import { DynamicObjectParams } from 'render';

export enum SDFShapes {
  Capsule = 1,
  Cylinder = 2,
  Torus = 3,
  Sphere = 4,
  Box = 5,
  Ground = 6,
}

export type PhysicsContext = {
  world: World;
  objs: DynamicObjectParams[];
  bodies: Body[];
}

///////////////////////////////////////////////////////////

export function phySetup(): PhysicsContext {
  const pc = {
    world: new World(),
    objs: [], // constant?
    bodies: [],
  } as PhysicsContext;
  // Setup our world
  pc.world.gravity.set(0, 0, -9.82); // m/s²
  pc.world.allowSleep = true;
  pc.world.defaultContactMaterial = new ContactMaterial(
    pc.world.defaultMaterial,
    pc.world.defaultMaterial,
    { friction: 1, restitution: .9 });
  return pc;
}

export function phyAddBody(ctx: PhysicsContext, obj: DynamicObjectParams) {
  // Dapao (and Cannon) operate on a XZY style system where most everything else is XYZ
  switch (obj.type) {
    case SDFShapes.Sphere: {
      const sphereBody = new Body({
        mass: 5,
        position: new Vec3(obj.pos[0], obj.pos[2], obj.pos[1]),
        // quaternion: new Quaternion(0, 0, 0, 0),
        shape: new Sphere(obj.params[0]),
      } as BodyOptions);
      ctx.objs.push(obj);
      ctx.bodies.push(sphereBody);
      ctx.world.addBody(sphereBody);
    } break;
    case SDFShapes.Box: {
      const boxBody = new Body({
        mass: 5,
        position: new Vec3(obj.pos[0], obj.pos[2], obj.pos[1]),
        // quaternion: new Quaternion(0, 0, 0, 0),
        shape: new Box(new Vec3(obj.params[0], obj.params[1], obj.params[2]))
      } as BodyOptions);
      ctx.objs.push(obj);
      ctx.bodies.push(boxBody);
      ctx.world.addBody(boxBody);
    } break;
    case SDFShapes.Cylinder: {
      const capBody = new Body({
        mass: 5,
        position: new Vec3(obj.pos[0], obj.pos[2], obj.pos[1]),
        shape: new Cylinder(
          obj.params[1],
          obj.params[1],
          obj.params[0],
          8)
      });
      ctx.objs.push(obj);
      ctx.bodies.push(capBody);
      ctx.world.addBody(capBody);
    } break;
  }
}

export function phyAddStaticBody(ctx: PhysicsContext) {
  // Create a plane
  const groundBody = new Body({
    mass: 0, // mass == 0 makes the body static
  });
  const groundShape = new Plane();
  // ground shape defaults to (0,0,1)
  groundBody.addShape(groundShape);
  ctx.world.addBody(groundBody);
}

const fixedTimeStep = 1.0 / 60.0; // seconds
const maxSubSteps = 3;
let lastTime = 0;
export function phyStep(ctx: PhysicsContext, time: number = 0) {
  if (lastTime !== undefined) {
    const dt = (time - lastTime) / 1000;
    ctx.world.step(fixedTimeStep, dt, maxSubSteps);
  }
  lastTime = time;
}
