const buildSettings = {
  entryPoints: ["src/main.ts"],
  bundle: true,
  minify: process.env.NODE_ENV !== "development",
  minifyWhitespace: process.env.NODE_ENV !== "development",
  sourcemap: process.env.NODE_ENV === "development",
  outfile: `dist/main.js`,
  plugins: [],
  loader: {
    ".glsl": "text",
    ".svg": "text",
  },
  define: {
    "process.env.NODE_ENV": `"${process.env.NODE_ENV}"`,
  },
  watch: process.env.NODE_ENV === "development",
};

require("esbuild")
  .build(buildSettings)
  .catch(() => process.exit(1));
