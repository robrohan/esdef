import { CasterContext } from 'core';

let screenBufferID: WebGLBuffer = undefined;

class ObjectLocation {
  type: WebGLUniformLocation;
  pos: WebGLUniformLocation;
  quat: WebGLUniformLocation;
  params: WebGLUniformLocation;
  color: WebGLUniformLocation;
}
class Locations {
  resolutionID: WebGLUniformLocation;
  rayOriginID: WebGLUniformLocation;
  rayDirectionID: WebGLUniformLocation;
  timeID: WebGLUniformLocation;
  posLoc: WebGLUniformLocation;
  objects?: ObjectLocation[];
}

export type DynamicObjectParams = {
  type: number;
  pos: [number, number, number];
  quat: [number, number, number, number];
  params: [number?, number?, number?, number?];
  color: [number?, number?, number?, number?];
}

function setObjectProperites(ctx: CasterContext, program: WebGLProgram, locations: Locations, idx: number, props: DynamicObjectParams) {
  lookupObjectLocation(ctx, program, idx, locations, 'type');
  lookupObjectLocation(ctx, program, idx, locations, 'pos');
  lookupObjectLocation(ctx, program, idx, locations, 'quat');
  lookupObjectLocation(ctx, program, idx, locations, 'params');
  lookupObjectLocation(ctx, program, idx, locations, 'color');

  ctx.gl.uniform1i(locations.objects[idx].type, props.type);
  ctx.gl.uniform3fv(locations.objects[idx].pos, props.pos);
  ctx.gl.uniform4fv(locations.objects[idx].quat, props.quat);
  ctx.gl.uniform4fv(locations.objects[idx].params, props.params);
  ctx.gl.uniform4fv(locations.objects[idx].color, props.color);
}

function lookupObjectLocation(ctx: CasterContext, program: WebGLProgram, idx: number, locations: Locations, prop: string) {
  (<any>locations.objects[idx])[prop] = lookupLocation(ctx, program, `objects[${idx}].${prop}`);
}

function lookupLocation(ctx: CasterContext, program: WebGLProgram, prop: string): WebGLUniformLocation {
  return ctx.gl.getUniformLocation(program, prop);
}

function getLocations(ctx: CasterContext, program: WebGLProgram): Locations {
  const l = new Locations();
  l.resolutionID = ctx.gl.getUniformLocation(program, 'resolution');
  l.rayOriginID = ctx.gl.getUniformLocation(program, 'ro');
  l.rayDirectionID = ctx.gl.getUniformLocation(program, 'rd');
  l.timeID = ctx.gl.getUniformLocation(program, 'time');
  l.objects = new Array(256).fill(new ObjectLocation());
  return l;
}

export function render(ctx: CasterContext, program: WebGLProgram, dynamicObjects: DynamicObjectParams[]) {
  const gl = ctx.gl;
  if (screenBufferID == undefined) {
    screenBufferID = screenBuffer(gl);
  }

  gl.clearColor(0., 0., 0., 1.0);
  gl.clear(gl.DEPTH_BUFFER_BIT
    | gl.COLOR_BUFFER_BIT
    | gl.STENCIL_BUFFER_BIT);
  gl.viewport(0, 0,
    ctx.gl.drawingBufferWidth, ctx.gl.drawingBufferHeight);

  gl.useProgram(program);

  gl.bindBuffer(gl.ARRAY_BUFFER, screenBufferID);

  const locations = getLocations(ctx, program);

  gl.uniform2fv(locations.resolutionID,
    [ctx.canvas.clientWidth, ctx.canvas.clientHeight]);
  gl.uniform3fv(locations.rayOriginID, [0., 1., -8.]);
  gl.uniform3fv(locations.rayDirectionID, [0., 0., 1.]);
  gl.uniform1f(locations.timeID, performance.now() * .002);

  const dl = dynamicObjects.length;
  for (let i = 0; i < dl; i++) {
    setObjectProperites(ctx, program, locations, i, dynamicObjects[i]);
  }

  const posLoc = gl.getAttribLocation(program, 'position');
  gl.vertexAttribPointer(posLoc, 3, gl.FLOAT, false, 0, 0);

  // Setting this to nil makes it write to the canvas
  gl.bindFramebuffer(gl.FRAMEBUFFER, null);

  gl.enableVertexAttribArray(posLoc);

  gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
};

// Create a quad that we can use to render to the full screen.
function screenBuffer(gl: WebGLRenderingContext): WebGLBuffer {
  const glid = gl.createBuffer();
  const screenQuad = new Float32Array([
    -1.0, 1.0, 0,
    -1.0, -1.0, 0,
    1.0, 1.0, 0,
    1.0, -1.0, 0,
  ]);
  gl.bindBuffer(gl.ARRAY_BUFFER, glid);
  gl.bufferData(gl.ARRAY_BUFFER, screenQuad, gl.STATIC_DRAW);
  gl.bindBuffer(gl.ARRAY_BUFFER, null);

  return glid;
};


