import { CasterContext, glContext, glProgram } from 'core';
import { DynamicObjectParams, render } from 'render';
import { run, scene, obj, join, sphere, smoothUnion, draw, lights, light, ground, cube } from 'transpile';
import { phyAddBody, phyAddStaticBody, phySetup, PhysicsContext, phyStep, SDFShapes } from 'physics';

// @ts-ignore
import v from './shaders/vertex.glsl';
// @ts-ignore
import f from './shaders/sdFragment.glsl';

function resize(ctx: CasterContext) {
  const gl = ctx.gl;
  const width = 960; // 1280; // gl.canvas.clientWidth;
  const height = 540; // 720; //gl.canvas.clientHeight;
  if (gl.canvas.width != width || gl.canvas.height != height) {
    gl.canvas.width = width;
    gl.canvas.height = height;
  }
}

function compileScene(ctx: CasterContext, dynamicObjects: DynamicObjectParams[]) {
  const objarray: (()=>string)[] = [];
  for (let i = 0; i < dynamicObjects.length; i++) {
    objarray.push(draw(obj(i), dynamicObjects[i].color));
  }

  const t = [
    () => scene([
      // simple draw
      // draw(sphere([3, 1, 6], [1]), [0, 1, 1, 1]),

      ...objarray,

      // Drawing by with a anon function
      // () => {
      //   let buf = '';
      //   const cluster = [];

      //   cluster.push(sphere([0, Math.random() * 2, 0], [.1]));
      //   for (let z = 10; z < 20; z++) {
      //     for (let x = -4; x <= 4; x++) {
      //       cluster.push(sphere([x, Math.random() * 2, z], [.1]));
      //       cluster.push(combine());
      //     }
      //   }

      //   const o = join(cluster);
      //   buf += draw(o)();
      //   return buf;
      // },

      // more static draw
      // draw(
      //   join([
      //     sphere([.5, 1, 6], [1]),
      //     cube([-.5, 1, 6], [.75, .75, .75, 0.]),
      //     smoothUnion([.2]),
      //   ]),
      //   [.5, .15, .25, .8]
      // ),
      draw(ground([0, 0, 0])),
    ]),
    () => lights([
      light([1, 13, 0], [1, 1, 1, 1]),
      light([1, 5, -10], [1, 1, 1, 1]),
    ]),
  ];
  const sceneLights = run(t);

  let frag = f.toString().replace('// ##caster_scene##', sceneLights[0]);
  frag = frag.toString().replace('// ##caster_light##', sceneLights[1]);
  const prog = glProgram(ctx.gl, frag, v);
  return prog;
}

/////////////////////////////////////////////////////////////////////////////////

/** From physics state to render state */
function update(time: number, ctx: CasterContext, phyCtx: PhysicsContext): void {
  const len = phyCtx.objs.length;
  for (let i = 0; i < len; i++) {
    const b = phyCtx.bodies[i];
    const o = phyCtx.objs[i];

    o.pos[0] = b.position.x;
    o.pos[1] = b.position.z;
    o.pos[2] = b.position.y;

    o.quat[0] = b.quaternion.x;
    o.quat[1] = b.quaternion.z;
    o.quat[2] = b.quaternion.y;
    o.quat[3] = b.quaternion.w;

    if (o.type === SDFShapes.Cylinder) {
      o.pos[1] -= (o.params[0] * .5);
    }
  }
}

/////////////////////////////////////////////////////////////////

console.log('%c caster', 'color:red; background-color: white;');
const dynamicObjects: DynamicObjectParams[] = [
  {
    type: SDFShapes.Box,
    pos: [0., 5., 0.],
    quat: [0., 0., 0., 1.],
    params: [.8, .8, .8, 0],
    color: [.2, 1., 1., .5],
  },
  {
    type: SDFShapes.Box,
    pos: [1., 7., 1.],
    quat: [0., 0., 0., 1.],
    params: [1., 1, 1, 0],
    color: [1., 1., 1., .5],
  },
  {
    type: SDFShapes.Box,
    pos: [1., 1., 0],
    quat: [0., 0., 0., 1.],
    params: [.5, .5, .5, 0.],
    color: [.75, .25, 1., .5],
  },
];

// const stress = (): DynamicObjectParams[] => {
//   const objs = [];
//   for (let k = 0; k < 4; k++) {
//     const r = Math.random();
//     const o = {
//       // type: (Math.round(Math.random())) ? SDFShapes.Sphere : SDFShapes.Box,
//       type: SDFShapes.Box,
//       pos: [Math.random() * 10, Math.random() * 10, Math.random() * 10],
//       quat: [0., 0., 0., 1.],
//       params: [r, r, r, 0],
//       color: [Math.random(), Math.random(), Math.random(), .5],
//     };
//     objs.push(o as DynamicObjectParams);
//   }
//   return objs;
// }
// const dynamicObjects = stress();

const ctx = glContext('#caster');
let prog = compileScene(ctx, dynamicObjects);
let phyCtx = phySetup();
// add the ground
phyAddStaticBody(phyCtx);
for (let i = 0; i < dynamicObjects.length; i++) {
  phyAddBody(phyCtx, dynamicObjects[i]);
}

const renderLoop = function (t?: number) {
  resize(ctx);

  update(t, ctx, phyCtx);
  render(ctx, prog, dynamicObjects);
  phyStep(phyCtx, t);

  requestAnimationFrame(renderLoop);
};
requestAnimationFrame(renderLoop);
