
let currentDocument: HTMLDocument;

export type CasterContext = {
  canvas: HTMLCanvasElement,
  gl: WebGLRenderingContext
}

export function setupDocument(doc: HTMLDocument) {
  currentDocument = doc;
};

export function getDocument(): HTMLDocument {
  return currentDocument || document;
};

export function glContext(canvasSelector = '#caster') {
  const canvas = getDocument().querySelector(canvasSelector) as HTMLCanvasElement;
  return setupCanvas(canvas);
};

export function setupCanvas(canvas: HTMLCanvasElement): CasterContext {
  const options = {
    alpha: false,
    antialias: false,
    depth: true,
    stencil: true,
    premultipliedAlpha: true,
    preserveDrawingBuffer: false,
    powerPreference: 'default',
  };

  let gl;

  if (canvas) {
    gl = canvas.getContext('webgl', options) as WebGLRenderingContext;

    if (!gl) {
      gl = canvas.getContext('experimental-webgl', options) as WebGLRenderingContext;
    }

    if (!gl) {
      throw Error('Your browser does not support WebGL');
    }
  } else {
    throw Error('Canvas is undefined');
  }

  gl.enable(gl.DEPTH_TEST);

  gl.enable(gl.CULL_FACE);
  gl.frontFace(gl.CCW);
  gl.cullFace(gl.BACK);

  gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
  gl.enable(gl.BLEND);

  return { canvas, gl };
};

export function glProgram(gl: WebGLRenderingContext, fragmentShaderText: string, vertexShaderText: string) {
  let program;
  if (gl) {
    // Shaders
    const vertexShader = gl.createShader(gl.VERTEX_SHADER);
    const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(vertexShader, vertexShaderText);
    gl.shaderSource(fragmentShader, fragmentShaderText);

    gl.compileShader(vertexShader);
    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
      throw Error('ERROR compiling vertex shader! '
        + gl.getShaderInfoLog(vertexShader));
    }

    gl.compileShader(fragmentShader);
    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
      throw Error('ERROR compiling fragment shader! '
        + gl.getShaderInfoLog(fragmentShader));
    }
    // Program
    program = gl.createProgram();
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);
    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
      throw Error('ERROR linking program! ' + gl.getProgramInfoLog(program));
    }
    gl.validateProgram(program);
    if (!gl.getProgramParameter(program, gl.VALIDATE_STATUS)) {
      throw Error('ERROR validating program! '
        + gl.getProgramInfoLog(program));
    }

    gl.deleteShader(vertexShader);
    gl.deleteShader(fragmentShader);
  }

  return program;
};


